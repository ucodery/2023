# PyGotham TV

This is the project that powers the website for
[PyGotham TV](https://2023.pygotham.tv).

## Contibuting

The PyGotham website is a static site generated by
[Jekyll](https://jekyllrb.com). If you'd like to contribute to it, you'll need
to have [Ruby](https://www.ruby-lang.org/en/documentation/installation/) (we use
version 3.0) and [Bundler](http://bundler.io). Once you have these installed,
you can install the project's requirements with

    $ bundle install

When you're ready to preview your changes, run

    $ bundle exec jekyll serve

then visit [http://localhost:4000](http://localhost:4000) to view the site.

### Adding an Announcement

Announcements are added by placing a new markdown file in the `_posts`
directory. Posts have two required front matter variables:

    title: The title of the post.
    date: The date and time when the post should be published.

Posts also support the following optional front matter variables:

    call_to_action: A URL, the existence of which will cause the post to appear
        on the home page as a call to action.
    image: An image to show at the top of the post.
    attribution: The URL that refers to the original location of the post.

Other Jekyll front matter variables may be supported. Refer to the
[documentation](https://jekyllrb.com/docs/posts/) for details.

Any content after the front matter will be used as the body of the post.

### Adding a Sponsor

Sponsors are added by placing a new markdown file in the `_sponsors` directory
and an image in the `uploads/sponsors` directory. The sponsor is described
through the following front matter variables:

    name: The name of the sponsor.
    tier: The sponsorship tier selected by the sponsor. The value should be a
        slugified version of the tier's name.
    site_url: The URL of the sponsor's website.
    logo: The name of the file in `uploads/sponsors` containing the sponsor's
        logo.
    twitter: The sponsor's preferred twitter handle for announcements.

Any content after the front matter will be used as the description of the
sponsor.

### Adding a Talk

Talks are added by placing a new markdown file in the `_talks` directory. The
talk is described through the following front matter variables:

    title: The title of the talk.
    speakers: A list of the names of the people presenting the talk. There will
        often only be one name in the list.
    level: The intended level of the audience of the talk.
    abstract: A brief overview of the talk.
    type: The type of the talk. The value can be anything, but the expected
        values are `talk`, `keynote`, `break`, and `talk`.

Talks also support the following optional front matter variables:

    presentation_url: The URL where the presentation (usually slides) of the
        talk can be found.
    video_url: The URL where the video of the talk has been placed online.
    slot: The date of time of the talk. While this is optional, it's required
        for it to appear in the schedule.
    room: The name of the room in which the talk is taking place. While this is
        optional, it's required for it to appear in the schedule.
    duration: The length, in minutes, of the talk.

Any content after the front matter will be used as the description of the talk.

### Adding a Speaker

Speakers are added by placing a new markdown file in the `_speakers` directory.
The speaker is described through the following front matter variables:

    name: The name of the presenter
    talks: A list of the titles of the talks being given by the speaker.

Any content after the front matter will be used as the speaker's bio.
