---
title: Sponsors
---

{% if site.sponsors.size == 0 %}
  [Be the first to sponsor {{ site.data.event.name }}!]({% link sponsors/prospectus.md %})
{% else %}
{% assign tiers = site.sponsors | tiered_sponsors: site.data.event.sponsor_tiers %}
<p class="text-center">
  Want to learn more about our sponsors?
  Fill out <a href="https://forms.gle/guPazLxRzzBFfd2t9">this short form</a>,
  and we'll make an introduction!
</p>
{% for tier in tiers %}
  {% assign sponsors = tier[1] | sort: 'name' %}
  {% if sponsors.size > 0 %}
  <h2>
    {{ tier[0] | titlecase }} {{ sponsors | size | pluralize: 'Sponsor' }}
  </h2>

  <ul class="sponsors row {{ tier[0] | slugify }}">
    {% for sponsor in sponsors %}
    <li class="justify-content-center col-xs-12 {% if tier[0] == site.data.event.sponsor_tiers[0] %}offset-md-3 col-md-6{% elsif tier[0] == site.data.event.sponsor_tiers[1] %}col-sm-6{% else %}col-sm-6 col-md-4{% endif %}">
      <a name="{{ sponsor['name'] | slugify }}"></a>

      <a href="{{ sponsor['site_url'] }}" class="logo">
        <img src="/uploads/sponsors/{{ sponsor['logo'] }}" alt="{{ sponsor['name'] }}">
      </a>

      <h4>{{ sponsor['name'] }}</h4>

      <a href="{{ sponsor['site_url'] }}">{{ sponsor['site_url'] | simplify_url }}</a>

      {{ sponsor.content }}
    </li>
    {% endfor %}
  </ul>
  {% endif %}
{% endfor %}
{% endif %}
