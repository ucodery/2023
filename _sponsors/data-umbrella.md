---
name: Data Umbrella
tier: community
site_url: https://www.dataumbrella.org
logo: data-umbrella.png
twitter: DataUmbrella
---
Data Umbrella is a Community for underrepresented persons in data science. We
organize data science and open source events for the community. Join our Meetup
group: <https://www.meetup.com/nyc-data-umbrella/>.
