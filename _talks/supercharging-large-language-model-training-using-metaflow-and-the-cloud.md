---
duration: 33
presentation_url:
room:
slot: 2023-10-07 12:35:00-04:56
speakers:
- Valay Dave
- Utkarsh Kashyap
title: Supercharging Large Language Model training using Metaflow and the Cloud
type: talk
video_url:
---

In the current landscape of Natural Language Processing (NLP), Large
Language Models (LLMs) like GPT-4 are leading the charge, necessitating
research into scalable, efficient training techniques accessible to the
majority. This rise of LLMs has heralded parameter-efficient training
techniques such as [Low-Rank Adaptation
(LoRA)](https://arxiv.org/pdf/2106.09685.pdf), which, while promising,
present [a complex challenge in their application and
experimentation](https://twitter.com/karpathy/status/1655994367033884672).
This presentation proposes a consistent, effective pattern for finetuning
LLMs reliably, leveraging [Metaflow](https://metaflow.org/), a human-
friendly ML infrastructure library, to showcase this pattern in action.
