---
duration: 18
presentation_url:
room:
slot: 2023-10-07 10:25:00-04:56
speakers:
- Mario Munoz
title: Python2Nite with Mario Munoz
type: talk
video_url:
---

*Python2Nite with Mario Munoz* is a late night talk show hosted by Python By Night's Mario
Munoz. In this week's episode, Mario goes back to the future of
hypermedia to talk about the resurgence of of an old technological paradigm
within the Python space. Could this be the RESTful zen that Python web
developers long for? Will he cover it from all... _angles_? Watch to see
his... _reaction_!
