---
duration: 24
presentation_url:
room:
slot: 2023-10-06 14:00:00-04:56
speakers:
- Jeremy Paige
title: Formalizing a Language
type: talk
video_url:
---

Python's grammar is the rules that define what is accepted as a legal
program. In order for text to get executed on a CPU it is first transformed
into valid instructions according to these rules. This talk will show
examples of these transformations as well as grammar pulled straight from
cpython source and picked apart so it is as easy to understand as Python
code.
