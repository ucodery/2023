---
duration: 18
presentation_url:
room:
slot: 2023-10-07 15:50:00-04:56
speakers:
- Reshama Shaikh
title: A Look at the Community Life Cycle in the Open Source Space
type: talk
video_url:
---

In 2023, the US Surgeon General issued a report on the “Our Epidemic of
Loneliness & Isolation: Healing Effects of Social Connection and Community.”
In this talk, we examine the importance of community and the experiences of
a few community organizers in the open source space. We explore getting
started, staying involved and leaving communities.
