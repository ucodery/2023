---
duration: 27
presentation_url:
room:
slot: 2023-10-06 10:50:00-04:56
speakers:
- Savannah Ostrowski
title: Accelerate your workflow from local Python prototype to the cloud
type: talk
video_url:
---

Have you ever struggled to get a Python application from your local machine
to the cloud? Had a hard time figuring out what infrastructure you need or
how to configure it for your app? Spent too much time researching how to set
up your editor or IDE for cloud development?

In this session, we'll talk about how to adapt flexible cloud development
application templates for your application and use the Azure Developer CLI
to go from local development environment to a fully-fledged application in
the cloud. Learn how to scaffold your application, customize a template, set
up your local development environment, provision infrastructure, deploy your
code, monitor your application health, and set up a CI/CD pipeline, all in a
couple of steps and just a few minutes.
