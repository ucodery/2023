---
duration: 30
presentation_url:
room:
slot: 2023-10-06 11:20:00-04:56
speakers:
- Cameron Riddell
title: Would Tufte Like Matplotlib?
type: talk
video_url:
---

Edward Tufte is a world renowned professor best known for pioneering the
field of data visualization. He thought deeply about how information is
communicated through visualizations and was one of the first to formalize
this knowledge. Notably, he introduced the concepts of `chartjunk`, `lie
factor`, `data-ink` ratio, and `data density` and many others which are all
widely discussed in the data visualizations today. While Tufte's work
continues to shape the growing field of data visualization, many of the
concepts he conveys originate from hand-drawn graphs.

Modern graphs are now computer generated, and with this in mind I seek to
answer the question "Would Tufte Like Matplotlib?" Since its first v1.0
release in 2010, `matplotlib` is the oldest Python-based plotting library
still widely used today. In more recent years, numerous other Python
plotting libraries have been created- `plotly`, `bokeh`, `plotnine`,
`seaborn`, which all have their own niches and usecases. Instead of directly
comparing `matplotlib` to its competitors, I will demonstrate the incredible
flexibility that `matplotlib` offers and how users can avoid confusion when
approaching the tool.

Specifically, I will discuss what `matplotlib` does best- the programmatic
construction of graphs as part of an ongoing workflow.
