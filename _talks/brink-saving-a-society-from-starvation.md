---
duration: 11
presentation_url:
room:
slot: 2023-10-07 10:00:00-04:56
speakers:
- Krishi Sharma
title: 'Brink: Saving a Society from Starvation'
type: talk
video_url:
---

Kawanda, a technologically advanced society on Planet Mirth, is on the brink
of starvation and they have little time before they run out of food to feed
their people. While they have sophisticated weaponry, and robotics, they
only just started using data and Python to understand their society. The key
challenge in food growth on their planet is that the unpredictable weather
patterns has made it hard for them to take the proper precautions for them
to keep the plants healthy.

So far, their tactics with known solutions and their strengths in robotics
has failed, and the leaders worry that their nation go extinct from
starvation. That is, until they find a stranded alien from a neighboring
planet that may just have the solution that they need to help them unlock
the secret to taming their temperamental climate. How can they predict the
weather of the future using the data they've collected? Will the alien from
Planet Earth be more help than harm?
