---
duration: 23
presentation_url:
room:
slot: 2023-10-06 13:20:00-04:56
speakers:
- Asher Sterkin
title: Green Data Processing with Python 3.12 Polars on ARM CPU
type: talk
video_url:
---

Starting with [version 3.11](https://docs.python.org/3/whatsnew/3.11.html#:~
:text=Python%203.11%20is%20between%2010,See%20Faster%20CPython%20for%20detai
ls), Python's performance has improved by 60%. Additionally, the Polars
library reportedly offers [2-5 times
faster](https://medium.com/cuenex/pandas-2-0-vs-polars-the-ultimate-battle-a
378eb75d6d1#:~:text=In%20terms%20of%20performance%2C%20Polars,with%20strings
%20(categorical%20features)) data processing than Pandas, while ARM CPUs are
said to be [60% more energy-efficient](https://aws.amazon.com/ec2/graviton/)
than Intel/AMD CPUs, with 20%-40% better performance.

In this talk, we will put these claims to the test using the AWS Graviton
3-based virtual desktop, the latest [Python
3.12](https://docs.python.org/3.12/whatsnew/index.html) version, and the
Polars library. Join us as we explore just how much more eco-friendly and
efficient data processing can be with these cutting-edge technologies.
