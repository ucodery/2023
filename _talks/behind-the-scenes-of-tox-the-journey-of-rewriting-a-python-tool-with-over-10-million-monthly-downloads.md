---
duration: 28
presentation_url:
room:
slot: 2023-10-06 15:00:00-04:56
speakers:
- Jürgen Gmach
title: 'Behind the Scenes of tox: The Journey of Rewriting a Python Tool with Over
  10 Million Monthly Downloads'
type: talk
video_url:
---

tox is a widely-used tool for automating testing in Python. In this talk, we
will go behind the scenes of the creation of tox 4, the latest version of
the tool. We will discuss the motivations for the rewrite, the challenges
and lessons learned during the development process. We will have a look at
the new features and improvements introduced in tox 4. But most importantly,
you will get to know the maintainers and their motivation.
