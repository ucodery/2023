---
duration: 44
presentation_url:
room:
slot: 2023-10-06 10:00:00-04:56
speakers:
- Julian Berman
title: 'Open Source: from Passion to Hobby to Work, or There and Back Again'
type: talk
video_url:
---

It has been almost 15 years since beginning to work on open source in the
Python community. What began as an attempt to scratch an itch (one needed
for a personal project) quickly -- surprisingly quickly -- became something
used by others. Nearly 10 years and multiple other projects later, the
little open source project went from a long-running side project and then,
thanks to the gracious support of Postman, a fully paid day job. It's still
somewhat uncommon to work on open source as a full-time job -- though it
benefits hugely both the community as well as the companies supporting
important projects they depend on. We'll discuss the road taken, some
lessons learned, some things that I still have no good answers for.
