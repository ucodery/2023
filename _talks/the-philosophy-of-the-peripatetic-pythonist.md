---
duration: 26
presentation_url:
room:
slot: 2023-10-07 10:50:00-04:56
speakers:
- Herve Aniglo
title: The Philosophy of the Peripatetic Pythonist
type: talk
video_url:
---

Look! Down on the Ground! Is it a dog?! Is it a car?! No, it is the
Peripatetic Pythonist! He travels around the world sharing his knowledge of
creating music with Python. He also shares his personal philosophy of life
through his musical demonstrations with technology.

Have you ever thought about converting code into music and making some
really awesome beats? You can do that with jythonMusic! jythonMusic uses
Python but it has functionalities like Java. You can create all types of
musical genres with this technology and I can show you how to do it through
a demo.
