---
duration: 23
presentation_url:
room:
slot: 2023-10-07 12:10:00-04:56
speakers:
- Hugo Bowne-Anderson
title: Human-Friendly, Production-Ready Data Science with Metaflow
type: talk
video_url:
---

Over the past five years, many new tools have emerged in the field of MLOps
and the existing ones have matured. Yet, there is not a clear picture of a
canonical stack for productive data science and ML organizations. Through
our work with open-source Metaflow, which was started at Netflix in 2017, we
have had an opportunity to talk to hundreds of companies at various levels
of maturity when it comes to ML infrastructure.

In this talk, we will provide an overview of key pain points that we have
observed - both technical and organizational - and suggest how the evolving
landscape of tooling can help address these challenges.
