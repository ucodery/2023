---
duration: 25
presentation_url:
room:
slot: 2023-10-06 15:30:00-04:56
speakers:
- Hugo Bessa
title: 'Don''t rely in memory: knowledge management for eng teams'
type: talk
video_url:
---

This talk delves into the crucial topic of knowledge management for
engineering teams. We will explore the inherent limitations of relying
solely on individual memory and uncover effective strategies to overcome
them.

During the session, we will highlight the risks associated with memory-based
knowledge retention, such as information loss due to turnover or scaling
challenges. By emphasizing the importance of capturing and organizing
knowledge, we will showcase how knowledge management systems can foster
collaboration, boost productivity, and facilitate informed decision-making.

Practical examples and valuable insights will be shared to guide
participants in implementing successful knowledge management practices.
Topics covered include identifying valuable how to organize knowledge,
selecting suitable knowledge management tools, establishing effective
documentation processes, and how to fit sync knowledge sharing into a
routine. Attendees will gain a deeper understanding of methodologies like
wikis, code repositories, and internal documentation platforms, as well as
best practices for their adoption.
