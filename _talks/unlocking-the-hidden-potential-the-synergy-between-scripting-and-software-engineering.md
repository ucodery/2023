---
duration: 30
presentation_url:
room:
slot: 2023-10-07 13:45:00-04:56
speakers:
- Anuj Menta
title: 'Unlocking the Hidden Potential: The synergy between Scripting and Software
  Engineering'
type: talk
video_url:
---

In the world of programming, scripting languages have long played a vital
role in automating tasks, prototyping ideas, and enabling rapid development.
However, the practices and principles that govern traditional software
engineering are often overlooked or underutilized within the scripting
community. This talk aims to bridge this gap and explore the valuable
lessons that scripting can learn from software engineering.

We will examine how adopting software engineering principles can lead to
improved code quality, maintainability, scalability, and overall development
efficiency in scripting projects. Attendees can expect to gain insights into
several key areas where scripting can benefit from software engineering
methodologies

Join me for an enlightening discussion on how scripting can learn from
software engineering, and unlock new avenues for growth and success in your
scripting projects.
