---
title: Voting is Open through May 26
date: 2023-05-18 08:00:00 -0400
---

Starting today, you can [cast your votes](https://cfp.pygotham.tv/vote) for
the short films and talks you want to see at this year's conference. We will
use this input to build the best schedule possible. Voting is open now
through May 26th, anywhere on Earth.  Thank you in advance for helping us
build the conference schedule you want to see.
