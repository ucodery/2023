---
title: "Keynote Speaker: Reshama Shaikh"
date: 2023-06-23 00:00:00 -0400
image: /uploads/posts/reshama-shaikh.jpg
excerpt_separator: <!--more-->
---

[Reshama Shaikh] is a statistician/data scientist based in New York City.
Reshama is the Director of Data Umbrella and an organizer for NYC PyLadies.
<!--more--> She is also on the Contributing Teams for scikit-learn and PyMC. She
was awarded the [Community Leadership Award from NumFOCUS in 2019], and is a
[Python Software Foundation Fellow].

[Reshama Shaikh]: https://www.linkedin.com/in/reshamas/
[Community Leadership Award from NumFOCUS in 2019]: https://reshamas.github.io/on-receiving-2019-community-leadership-award-from-numfocus/
[Python Software Foundation Fellow]: https://pyfound.blogspot.com/2022/07/announcing-python-software-foundation.html
