---
title: Talk schedule now available!
date: 2023-09-24 00:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham 2023 [schedule](/talks/schedule/) is now available. Join us for
thirty presentations including keynotes, tech talks, and short films. Videos
will be streamed on October 6th and 7th, beginning at 10:00AM each day with
repeat showings at 6:00PM EDT (UTC-4:00).

As always, PyGotham is a community effort. Thank you to everyone who proposed a
talk, voted on talks, and signed up for the program committee.
