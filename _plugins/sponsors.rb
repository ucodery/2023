require 'liquid'

module Sponsors
  def tiered_sponsors(sponsors, tiers)
    return Hash[ tiers.collect { |t| [t, sponsors.select { |s| s['tier'].include? t }] }]
  end
end

Liquid::Template.register_filter(Sponsors)
