---
title: Talk Schedule
body_class: schedule
---

<p>
  All times posted are in America/New_York time.
  <a href="https://time.is/New_York">Click here</a>
  to view the current time in New York and convert to different time zones.
</p>

<p>
  To accommodate viewers in different time zones, PyGotham TV talks will be
  aired twice per day.
</p>

{% assign talks = site.talks %}
{% assign delayed_talks = talks | copy_with_delay %}
{% assign talks_and_delayed_talks = talks | concat: delayed_talks %}
{% assign slotted_talks = talks_and_delayed_talks | where_exp: 'item', 'item.slot' %}
{% if slotted_talks.size > 0 %}

{% assign days = slotted_talks | slotted_schedule: site.data.event.venue.rooms %}
{% assign days = days | sort %}
{% for day in days %}
  <h2 id="day-{{ forloop.index }}">{{ day[0] | date: '%A, %B %-d' }}</h2>

  {% assign slots = day[1] | sort %}
  <ol class="schedule">
    {% for slot in slots %}
      <li>
        <div class="row">
          <div class="time col-md-1">
            {{ slot[0] | date: '%-I:%M%P' }}
          </div>
          <div class="slots col-md-11">
            {% for talk in slot[1] %}
              <div class="{{ talk.type }}">
                {% if talk.speakers.size > 0 %}
                  <a href="{{ talk.url }}">
                {% endif %}

                <h4>{{ talk.title }}</h4>

                <span class="location">{{ talk.room }}</span>

                <span class="presenters">{{ talk.speakers | join: ', ' }}</span>

                {% if talk.speakers.size > 0 %}
                  </a>
                {% endif %}
              </div>
            {% endfor %}
          </div>
        </div>
      </li>
    {% endfor %}
  </ol>
{% endfor %}

{% endif %}
