---
name: Herve Aniglo
talks:
- "The Philosophy of the Peripatetic Pythonist"
---
Herve Viho Aniglo was born in West Africa but grew up in Memphis, TN.
Growing up, he always had a fascination for technology and different
cultures and customs. Herve is very involved in the organizations, National
Society of Black Engineers, National Black MBA Association, and Black Data
Processing Associates. During his free time, he likes to mentor the youth
about technology and coding and enjoys traveling. He has many contacts from
all around the world and he is not afraid to share them with anybody. He
also enjoys reading comic books, playing videogames, and watching TV shows.
He received his Bachelors degree in Computer Science and his MBA from the
University of Memphis. Herve is definitely living up to his name as an Army
Warrior Chief Who Cannot Stand Still! He is never a stranger and is never
afraid to strike a conversation with anyone.
