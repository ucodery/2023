---
name: Cameron Riddell
talks:
- "Would Tufte Like Matplotlib?"
---
Cameron Riddell is a leading expert on data analysis, having mastered
analytic and visualization tools such as pandas, Matplotlib, and Bokeh. With
his background in academia and keen understanding of how people learn,
Cameron delivers key insights to corporate and individual clients, helping
them design and maintain robust systems with greater efficiency, accuracy,
and reliability.

Cameron is also a prolific presenter on data analysis and scientific
computing. In addition to attending his courses on O’Reilly Online Learning
you’ll want to check out his weekly blog, frequent public seminars, and
Discord discussions, all of which you can find linked on the Course
Resources page.

As with every course from Don’t Use This Code, you can expect Cameron’s
instruction to pack a lot of content into a short amount of time! You’ll
want to stay for the entire session to see how it all comes together, and
then refer back to the recording to take in all of the detail.
