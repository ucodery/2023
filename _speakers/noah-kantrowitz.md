---
name: Noah Kantrowitz
talks:
- "Swiss Army Django: Small Footprint ETL"
---
Noah Kantrowitz is a web developer turned infrastructure automation
enthusiast, and all around engineering rabble-rouser. By day he runs
infrastructure at Geomagical/IKEA and by night he makes candy and stickers.
He is an active member of the DevOps community, and enjoys merge commits,
cat pictures, and beards.
