---
name: Bard Fetler
talks:
- "Large Language Models Are Biased And Inaccurate.  Can They Be Improved?"
---
Bard is a machine learning engineer / data scientist with 20+ years
experience in software.  His current projects include medical entity
detection and studying LLMs.
