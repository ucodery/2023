---
name: Tadeh Hakopian
talks:
- "The Lost Art of Diagrams: Making Complex Ideas Easy to See with Python"
---
With a background in Architecture and Development Tadeh has supported
building large scale projects with diverse teams for over 10 years. By
promoting open dialogue, systems thinking, and long term planning he has
improved operational efficiencies in all his endeavors. He is a course
author, trainer, and open source contributor as well as a speaker at
national conferences in technology, architecture, design. He loves to talk
about the new possibilities with technology and innovation at everyone’s
disposal while building communities along the way.
