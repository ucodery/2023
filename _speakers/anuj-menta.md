---
name: Anuj Menta
talks:
- "Unlocking the Hidden Potential: The synergy between Scripting and Software Engineering\""
---
Anuj holds a Masters in Computing, Entrepreneurship and Innovation from NYU
and Masters in Mathematics and Computing from IIT Kharagpur. He loves
playing with numbers, data and code and finds a lot of interest in working
in fields that use all the three things he loves. He loves to build products
and tools over the weekend. He also loves speedcubing and tennis.
